[%bs.raw {|require('./container.css')|}];

let component = ReasonReact.statelessComponent("Container");

let make = (children) => {
  ...component,
  render: (_self) =>
    <div className="Container">
      (ReasonReact.arrayToElement(children))
    </div>
};
